#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Simple utility for dealing with API limits when downloading an XML dump from MediaWiki.
"""

import sys
import io
import http
import argparse
import requests
from urllib.parse import urlencode
from xml.etree import ElementTree


def batch(iterable, n):
	current_batch = []
	for i in iterable:
		current_batch.append(i)
		if len(current_batch) >= n:
			yield current_batch
			current_batch = []
	if current_batch:
		yield current_batch


parser = argparse.ArgumentParser(description = __doc__)
parser.add_argument('wiki_domain', type = str, help = 'Wiki domain')
parser.add_argument('page_list', type = str, help = 'Name of file with the '
	+ 'pages to dump, one title per line')
parser.add_argument('--debug', action='store_true', help = 'Debug output')
args = parser.parse_args()
if args.debug:
	http.client.HTTPConnection.debuglevel = 1


ua = 'one-off/gtisza@wikimedia.org'
ns = 'http://www.mediawiki.org/xml/export-0.11/'

ElementTree.register_namespace('', ns)
first_xml = current_xml = None
with open(args.page_list, 'r') as pages:
	# chunk to 50 pages which is the limit of titles for non-privileged API requests
	for page_batch in batch(pages, 50):
		# use post to get around URL length limits, just in case
		r = requests.post( f'https://{args.wiki_domain}/w/api.php',
			headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
				'User-Agent': ua,
			},
			data = urlencode({
				'action': 'query',
				'export': '1',
				'exportnowrap': '1',
				'exportschema': '0.11',
				'titles': '|'.join(page_batch).replace('\n', ''),
			}, safe='|'),
			stream = True,
		)
		r.raise_for_status()
		if not first_xml:
			first_xml = ElementTree.parse(io.StringIO(r.text))
		else:
			current_xml = ElementTree.parse(io.StringIO(r.text))
			pages = current_xml.getroot().findall('page', {'': ns})
			first_xml.getroot().extend(pages)
	first_xml.write(sys.stdout, encoding='unicode')
